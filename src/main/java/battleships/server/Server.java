package battleships.server;

import battleships.common.NetworkMessages;
import java.io.*;
import java.net.*;

/**
 * Server establishes 2 network socket connections. Allows client/server type
 * communications via PrintWriter, BufferedReader(InputStreamReader).
 * @author Joe Roberts
 */
public class Server implements NetworkMessages{
    private Socket player1, player2;
    private final int PORT = 8000;
    private ServerSocket serverSocket = null;
    private Session game;
    private PrintWriter toPlayer1;
    private PrintWriter toPlayer2;
    private BufferedReader fromPlayer1;
    private BufferedReader fromPlayer2;
    
    /**
     * Self calling
     * @param args 
     */
    public static void main(String[] args){
        new Server();
    }
    
    /**
     * Constructs and initialises server and game session.
     */
    public Server(){
        
        try {
            serverSocket = new ServerSocket(PORT);
            System.out.println("server live\n");
                       
            while (true){
                startServer();
                startNewGameSession();
            }
            
        }catch (BindException be){
            
            System.out.println("Unable to establish network "
                    + "\nPlease close all other applications running on port : " + PORT);
            
            System.exit(0);
        
        }catch (IOException ex) {
            System.out.println("Problem starting server");
            System.exit(0);
            
        }//end of try catch block
        
    }//END OF CONSTRUCTOR
    
    /*
     * accepts and sets 2 socket connections
     * @throws IOException 
     */
    private void establishNetwork()throws IOException{
        
        if(player1 == null){//check if player is already connected
            
            player1 = serverSocket.accept();
            
            System.out.println("Player 1 has joined session\n");
            
            toPlayer1 = new PrintWriter(player1.getOutputStream());
            
            fromPlayer1 = new BufferedReader(
                    new InputStreamReader(player1.getInputStream()));
            
            outgoingMessage(PLAYER1ID, PLAYER1ID);//sends player id to player1
        }
        
        if(player2 == null){//check if player is already connected
            
            player2 = serverSocket.accept();
            
            System.out.println("Player 2 has joined session\n");
            
            toPlayer2 = new PrintWriter(player2.getOutputStream());
                            
            fromPlayer2 = new BufferedReader(
                new InputStreamReader(player2.getInputStream()));
            
            outgoingMessage(PLAYER2ID, PLAYER2ID);//sends player id to player1
        }
    }//END OF establishNetwork METHOD
    
    /*
     * establishes socket connection and initialises input and output channels
     */
    private void startServer(){
        
        try{
            
            if(player1 == null || player2 == null){//check if both players are connected
                
                System.out.println("waiting for players to join.....\n");
                establishNetwork();
                
            }//end of if statement
            
        }catch (IOException ex) {
        
            System.out.println("Unable to establish network \nPlease try again");
            System.exit(0);
        }//end of try catch block
    }//END OF startServer METHOD
    
    /*
     * restarts the server and game session 
     */
    public void reStartServer(){
        notifyAllConnections();
        startServer();
        startNewGameSession();
    }
    
    /**
     * Notifies all connections of socket failure to restart the game.
     * checks which socket is closed and sends error message to the other socket.
     * In the event that both sockets are closed, both sockets are assigned a null value.
     */
    public void notifyAllConnections(){
        
        if(game.turn == 0){
            
            try {    
                
                fromPlayer1.readLine();
                sendMessage(toPlayer1,ERROR);
                player2 = null;
                System.out.println("can not communicate with player 2");
                
            }catch (IOException ex) {
                
                try {
                    
                    sendMessage(toPlayer2,ERROR);
                    
                } catch (SocketException ex1) {
                    
                    System.out.println("can not communicate with player 2");
                    player2 = null;
                    
                }
                player1 = null;            
                System.out.println("can not communicate with player 1");

            }
        }else{
            
            try {
                
                fromPlayer2.readLine();
                sendMessage(toPlayer2,ERROR);
                player1 = null;
                System.out.println("can not communicate with player 1");
                
            }catch (IOException ex) {
                
                try {
                    
                    sendMessage(toPlayer1,ERROR);
                    
                } catch (SocketException ex1) {
                    
                    System.out.println("can not communicate with player 1");
                    player1 = null;
                }
                player2 = null;            
                System.out.println("can not communicate with player 2");

            }
        }
    }
    
    /*
     * starts new game session
     */
    private void startNewGameSession(){
        game = new Session(this);
        game.startGameSession();
    }
    
    /*
     * sends String through specified socket output channel
     * @param outgoing
     * @param message 
     */
    private void sendMessage(PrintWriter outgoing, String message)throws SocketException{

        outgoing.println(message);

        outgoing.flush();

        if(outgoing.checkError()){
            throw new SocketException();

        }
    }//END OF sendMessage METHOD
    
    /*
     * waits for network message from specified socket input channel
     * @param incoming
     * @return 
     */
    private String recieveMessage(BufferedReader incoming)throws SocketException, IOException{
        
        String message = incoming.readLine();
        
        return message;
    }//END OF recieveMessage METHOD
    
    /**
     * Sends outgoing network message to specified socket. Used in conjunction 
     * with interface: NetworkMessages
     * @param to
     * @param message
     * @throws java.net.SocketException
     * @see NetworkMessages
     */
    public void outgoingMessage(String to, String message) throws SocketException{
        switch(to){
            case PLAYER1ID:
                sendMessage(toPlayer1, message);
                break;
            case PLAYER2ID:
                sendMessage(toPlayer2, message);
                break;
            default:
                break;
        }//end of switch
    }//END OF outgoingMessage METHOD
    
    /**
     * Waits for incoming message from specified socket. used in conjunction 
     * with interface: NetworkMessages.
     * @param from
     * @return 
     * @throws java.net.SocketException 
     * @see NetworkMessages
     */
    public String incomingMessage(String from) throws SocketException, IOException{
        String message = "";
        switch(from){
            case PLAYER1ID:
                message = recieveMessage(fromPlayer1);
                break;
            case PLAYER2ID:
                message = recieveMessage(fromPlayer2);
                break;
            default:
                break;
        }//end of switch
        
        return message;
        
    }//END OF incomingMessage METHOD
}//END OF CLASS
