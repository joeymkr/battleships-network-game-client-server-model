package battleships.server;

import battleships.common.NetworkMessages;
import java.io.*;
import java.net.SocketException;

/**
 * Session for specific use in Battleships. controls game logic server side 
 * and UI client side. Uses Server to communicate across network. To be used 
 * in conjunction with Client/Server systems incorporating the NetworkMessages 
 * interface.
 * @author Joe Roberts
 * @see Server
 * @see NetworkMessages
 */
public class Session implements NetworkMessages{
    private final Server networkChannel;
    private String[][] player1Grid = new String[6][6];
    private String[][] player2Grid = new String[6][6];
    public int turn;
    
    private int p1Life = 0;
    private int p2Life = 0;
    private boolean gameOver = false;
    
    /**
     * Construct and initialises Server instance and 2 sockets
     * @param networkChannel Server
     */
    public Session(Server networkChannel){
        this.networkChannel = networkChannel;
        
    }//END OF CONSTRUCTOR
    
    /**
     * Starts a new game
     */
    public void startGameSession(){
        try{
            while(true){
                gameOver = false;
                turn = 0;

                for (int i = 0; i < 6;i++){

                    for (int n = 0; n < 6; n++){

                        player1Grid[i][n] = EMPTY;
                        player2Grid[i][n] = EMPTY;

                    }//end of inner for loop
                }//end of outer for loop

                System.out.println("new Session");


                gamePrep();
                runGame();
            }
        }catch(NumberFormatException nfe){
            System.out.println("Client Synchronisation error\nRestarting server");
            networkChannel.reStartServer();
        }catch(SocketException se){
            System.out.println("Connection to client has been lost\nRestarting server");
            networkChannel.reStartServer();
        }catch (Exception ex) {
            System.out.println("error");
            networkChannel.reStartServer();
        }//end of try catch block
        
    }//END OF startGameSession METHOD
    
    /*
     * Tells clients to begin selection process and send battleship positions.
     * @throws NumberFormatException
     * @throws SocketException
     * @throws Exception 
     */
    private void gamePrep() throws NumberFormatException, SocketException,IOException, Exception{
              
        positionFleet(player1Grid, PLAYER1ID,PLAYER1ID);
        
        positionFleet(player2Grid, PLAYER2ID,PLAYER2ID);
        
        p1Life = 9;
        p2Life = 9;
        
        networkChannel.outgoingMessage(PLAYER1ID, START);
        networkChannel.outgoingMessage(PLAYER2ID, START);
        
    }//END OF gamePrep METHOD
    
    /*
     * Intiates local grids with players battleship positions
     * @param grid
     * @param incoming
     * @param outgoing
     * @throws NumberFormatException
     * @throws SocketException
     * @throws IOException
     * @throws Exception 
     */
    private void positionFleet(String[][] grid, String incoming, String outgoing) throws NumberFormatException, SocketException,IOException,Exception {
        for (int i = 0; i<9;i++){
            int row = Integer.parseInt(networkChannel.incomingMessage(incoming));
            int column = Integer.parseInt(networkChannel.incomingMessage(incoming));
            grid[row][column] = OCCUPIED;
            //networkChannel.outgoingMessage(outgoing, CONTINUE);
        }
    }
    
    /*
     * Runs in game play loop
     * @throws SocketException
     * @throws IOException
     * @throws Exception 
     */
    private void runGame()throws SocketException, IOException, Exception{
        while (p1Life > 0 && p2Life > 0){
            
            while (turn == 0 && !gameOver){
                //1st player move
                int row = Integer.parseInt(networkChannel.incomingMessage(PLAYER1ID));
                
                int column = Integer.parseInt(networkChannel.incomingMessage(PLAYER1ID));
                
                String status = EMPTY;
                
                switch (player2Grid[row][column]){
                    case EMPTY:
                        networkChannel.outgoingMessage(PLAYER2ID, CONTINUE);
                        networkChannel.outgoingMessage(PLAYER2ID, ""+row+"");
                        networkChannel.outgoingMessage(PLAYER2ID, ""+column+"");
                        networkChannel.outgoingMessage(PLAYER2ID, MISS);
                        networkChannel.outgoingMessage(PLAYER1ID, MISS);
                        turn = 1;
                        break;
                        
                    case OCCUPIED:
                        networkChannel.outgoingMessage(PLAYER1ID, HIT);
                        networkChannel.outgoingMessage(PLAYER2ID, WAIT);
                        networkChannel.outgoingMessage(PLAYER2ID, ""+row+"");
                        networkChannel.outgoingMessage(PLAYER2ID, ""+column+"");
                        networkChannel.outgoingMessage(PLAYER2ID, HIT);
                        status = HIT;
                        p2Life--;
                        
                        if(p2Life == 0){
                            networkChannel.outgoingMessage(PLAYER1ID, END);
                            gameOver = true;
                        }else {
                            networkChannel.outgoingMessage(PLAYER1ID, CONTINUE);
                        }
                        
                        break;
                        
                    default:
                        System.out.println("Error checking player2 grid");
                        break;
                        
                }//end of switch
                
                player2Grid[row][column] = status;
                
            }//end of inner while loop
            
            if(p2Life == 0){
                networkChannel.outgoingMessage(PLAYER1ID, PLAYER1_WON);
                networkChannel.outgoingMessage(PLAYER2ID, PLAYER1_WON);
                break;
            }
            
            while (turn == 1 && !gameOver){
                //2nd player move
                int row = Integer.parseInt(networkChannel.incomingMessage(PLAYER2ID));
                
                int column = Integer.parseInt(networkChannel.incomingMessage(PLAYER2ID));
                
                String status = EMPTY;
                
                switch (player1Grid[row][column]){
                    case EMPTY:
                        networkChannel.outgoingMessage(PLAYER1ID, CONTINUE);
                        networkChannel.outgoingMessage(PLAYER1ID, ""+row+"");
                        networkChannel.outgoingMessage(PLAYER1ID, ""+column+"");
                        networkChannel.outgoingMessage(PLAYER1ID, MISS);
                        networkChannel.outgoingMessage(PLAYER2ID, MISS);
                        turn = 0;
                        break;
                        
                    case OCCUPIED:
                        networkChannel.outgoingMessage(PLAYER2ID, HIT);
                        networkChannel.outgoingMessage(PLAYER1ID, WAIT);
                        networkChannel.outgoingMessage(PLAYER1ID, ""+row+"");
                        networkChannel.outgoingMessage(PLAYER1ID, ""+column+"");
                        networkChannel.outgoingMessage(PLAYER1ID, HIT);
                        status = HIT;
                        p1Life--;
                        
                        if(p1Life == 0){
                            networkChannel.outgoingMessage(PLAYER2ID, END);
                            gameOver = true;
                            break;
                        }else{
                            networkChannel.outgoingMessage(PLAYER2ID, CONTINUE);
                        }    
                        
                        break;
                        
                    default:
                        System.out.println("Error checking player1 grid");
                        break;
                        
                }//end of switch
                
                player1Grid[row][column] = status;
                
            }//end of inner while loop
            
            if(p1Life == 0){
                networkChannel.outgoingMessage(PLAYER1ID, PLAYER2_WON);
                networkChannel.outgoingMessage(PLAYER2ID, PLAYER2_WON);
                break;
            }
        }//end of while loop
        
        System.out.println("end of game");
    }//END OF runGame METHOD
}//END OF CLASS
