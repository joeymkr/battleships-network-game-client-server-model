package battleships.client;

import battleships.common.NetworkMessages;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.SocketException;

/**
 * Controller for specific use in Battleships. controls game logic and UI client
 * side. Uses Client to communicate across network. To be used in conjunction 
 * with Client/Server systems incorporating the NetworkMessages interface.
 * @author Joe Roberts
 * @see Client
 * @see NetworkMessages
 */
public class Controller implements Runnable, NetworkMessages{
    private boolean turn = false;
    private boolean waiting = true;
    private boolean gameInPlay;
    private final GameBoard GUI;
    private int playerID = 0;
    private Client networkChannel;
    private boolean selectionMode;
    public Battleship[] fleet;
    
    /**
     * Self calling
     * @param args 
     */
    public static void main(String[] args){
        new Controller(new Client());
    }
    
    /**
     * Constructs and initialises this. Sets new client to this network channel.
     * Creates new instance of GameBoard.
     * @param netChan 
     */
    public Controller(Client netChan){
        this.networkChannel = netChan;
        
        //sets up User Interface
        GUI = new GameBoard(this);
        gameInPlay = false;
        selectionMode = false;
        fleet = new Battleship[3];
        
    }//END OF CONSTRUCTOR
    
    /**
     * Checks if Client is Connected and creates new socket connection is not.
     */
    public void establishNetworkConnection(){
        
        if(!networkChannel.isConnected()){
            networkChannel = new Client();
            if(networkChannel.connect()){
                setMessage("connected");
                setMessage("Press start to begin");
                GUI.controlPanel.enableStartButton(true);
                
            }
        }else {
            setMessage("Already Connected");
        }
    }//END OF establishNetworkConnection METHOD
    
    /**
     * Starts new battleship game. Begins new Thread(this)
     */
    public void startGame(){
        turn = false;
        fleet[0] = new Battleship("Destroyer", 2);
        fleet[1] = new Battleship("Submarine", 3);
        fleet[2] = new Battleship("Carrier", 4);
        GUI.controlPanel.enableStartButton(false);
        GUI.resetGrids();
        Thread game = new Thread(this);
        game.start();        
    }//END OF startGame METHOD
    
    /**
     * Overrides Runnable method. Enters Client side game loop.
     * @see Runnable
     */
    @Override
    public void run(){
        
        try {
            String playerIndex = "";
            if(playerID == 0){
                playerIndex = networkChannel.incomingNetworkMessage();
            } else if(playerID == 1){
                playerIndex = PLAYER1ID;
            }else {
                playerIndex = PLAYER2ID;
            }
            
            GUI.setGridEnabled(true, false);
            
            switch (playerIndex) {
                
                case PLAYER1ID:
                    playerID = 1;
                    setTurn(true);
                    GUI.setTitle("player 1");
                    setMessage("you are player 1\n"
                            + "Please Place Your Fleet.\n"
                            + "Select a ship, Select a tile.\n"
                            + "Move and Click mouse to select orientation\n"
                            + "Battle Stations when ready...");
                    break;
                    
                case PLAYER2ID:
                    playerID = 2;
                    GUI.setTitle("player 2");
                    setMessage("you are player 2\n"
                            + "Please Place Your Fleet.\n"
                            + "Select a ship, Select a tile.\n"
                            + "Move and Click mouse to select orientation\n"
                            + "Battle Stations when ready...");
                    break;
                    
                default:
                    System.out.println("error : " + playerIndex);
                    break;
            }//end of switch
            
            setSelectionMode(true);
            
            GUI.setGridEnabled(true, false);
            
            while(inSelectionMode()){
                sleep(1000);
            }
            
            sendFleetPositions();
            
            setMessage("Waiting for War Declaration");
            System.out.println(playerID);
            waitForInstruction();//MESSAGE IGNORED
            
            while (gameInPlay){
                
                switch (playerIndex) {
                    case PLAYER1ID:
                        
                        waitForMove();                        
                        sendMove();
                        waitForInstruction();
                        break;
                    case PLAYER2ID:
                        
                        waitForInstruction();
                        if(gameInPlay){
                            waitForMove();
                            sendMove();
                        }
                        break;
                }//end of switch
            }//end of while loop
            
        }catch (NumberFormatException nfe){
            setMessage("Game Synchronization failure");
            endGame();
            networkChannel.resetConnection();
        } catch (InterruptedException ex) {
            setMessage("Game Synchronization failure");
            endGame();
            networkChannel.resetConnection();
        }catch (SocketException se){
            setMessage("Connection Lost");
            endGame();
            networkChannel.resetConnection();
        } catch (IOException ex) {
            setMessage("Problem with communication channels");
            endGame();
            networkChannel.resetConnection();
        }catch(Exception e){
            setMessage("Unexpected error: shutting down");
            endGame();
            networkChannel.resetConnection();
        }//end of try catch block
        
    }//END OF RUN METHOD
    
    /*
     * Sends chosen battleship grid positions to server.
     * @throws SocketException
     * @throws IOException 
     */
    private void sendFleetPositions() throws SocketException, IOException {
        for(int counter = 0; counter < fleet.length; counter++){
            for(int i = 0; i<fleet[counter].getLength();i++){
                switch(fleet[counter].getOrientation()){
                    case NORTH:
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getX()-i));
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getY()));
                        break;
                    case SOUTH:
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getX()+i));
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getY()));
                        break;
                    case WEST:
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getX()));
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getY()-i));
                        break;
                    case EAST:
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getX()));
                        networkChannel.outgoingNetworkMessage(Integer.toString((int)fleet[counter].getBasePosition().getY()+i));
                        break;
                    default:
                        break;
                        
                }//end of switch
                
                
            }//end of inner for loop
            
        }//end of outer for loop
        
    }//END OF sendFleetPositions METHOD
    
    /**
     * Sets current selected ships base grid position in GameBoard
     */
    public void setCurrentShipBasePosition(){
        int ship = GUI.getCurrentSelection();
        int row = GUI.getSelectedRow();
        int column = GUI.getSelectedColumn();
        fleet[ship].setBasePosition(row, column);
        fleet[ship].setIsBasePositionSet(true);
        
    }//END OF setCurrentBasePosition METHOD
    
    /**
     * Resets current selected ships grid position to new position in GameBoard
     */
    public void resetShip(){
        int ship = GUI.getCurrentSelection();
        fleet[ship].setPreviousPosition(fleet[ship].getBasePosition(), fleet[ship].getOrientation());
        fleet[ship].setIsBasePositionSet(false);
        fleet[ship].setFullyPositioned(false);
        
        int r = (int)fleet[ship].getPreviousPosition().getX();
        int c = (int)fleet[ship].getPreviousPosition().getY();
        
        for (int i = 0; i < fleet[ship].getLength(); i++){
            
            switch (fleet[ship].getPreviousOrientation()){
                
                case NORTH:
                    GUI.unlockCell(r-i,c);
                    GUI.setPlayersCell(r-i,c,GUI.getImage("EMPTY"));
                    break;
                case SOUTH:
                    GUI.unlockCell(r+i,c);
                    GUI.setPlayersCell(r+i,c,GUI.getImage("EMPTY"));
                    break;
                case WEST:
                    GUI.unlockCell(r,c-i);
                    GUI.setPlayersCell(r,c-i,GUI.getImage("EMPTY"));
                    break;
                case EAST:
                    GUI.unlockCell(r,c+i);
                    GUI.setPlayersCell(r,c+i,GUI.getImage("EMPTY"));
                    break;
                default:
                    break;
                    
            }//end of switch
            
        }//end of for loop
        
    }//END OF resetShip METHOD
    
    /**
     * Appends JTextarea Text in ControlPanel
     * @param message String to display
     */
    public void setMessage(String message){
        GUI.controlPanel.displayMessage(message);
    }
    
    /**
     * Sets current ships grid orientation in GameBoard
     * @param direction 
     */
    public void setShipOrientation(String direction){
        int ship = GUI.getCurrentSelection();
        fleet[ship].setOrientation(direction);
    }
    
    /**
     * Sets current selected ships full position
     */
    public void placeShip(){
        int currentShip = GUI.getCurrentSelection();
        int row = (int)fleet[currentShip].getBasePosition().getX();
        int column = (int)fleet[currentShip].getBasePosition().getY();
        
        fleet[currentShip].setOrientation(GUI.getCurrentMouseDirection());
        fleet[currentShip].setFullyPositioned(true);
        
        for(int i = 0; i < fleet[currentShip].getLength(); i++){
            
            switch (fleet[currentShip].getOrientation()){
                
                case "NORTH":
                    GUI.lockCell(row-i, column);
                        break;
                case "EAST":
                    GUI.lockCell(row, column+i);
                    break;
                case "SOUTH":
                    GUI.lockCell(row+i, column);
                    break;
                case "WEST":
                    GUI.lockCell(row, column-i);
                    break;
                default:
                    break;
                    
            }//end of switch
        }//end of for loop
        
        if(hasFleetBeenPositioned()){
            GUI.controlPanel.setSubmissionButton(true);
        }
    }//END OF placeShip METHOD
    
    
    /*
     * Returns each battleships fully position state as a whole. 
     * @return 
     */
    private boolean hasFleetBeenPositioned(){
        for (Battleship ship: fleet){
            if(!ship.isPositioned()){
                return false;
            }
        }
        return true;
    }
    
    /*
     * waits for JFrame event in GameBoard
     * @throws InterruptedException 
     */
    private void waitForMove() throws InterruptedException{
        
        while (isWaiting()){
            Thread.sleep(100);
        }
        
        setWaiting(true);
    }
    
    /**
     * Returns games selection mode state
     * @return boolean
     */
    public boolean inSelectionMode(){
        return selectionMode;
    }
    
    /**
     * Sets this games selection mode.
     * @param mode boolean to be assigned to this selection mode state
     */
    public void setSelectionMode(boolean mode){
        selectionMode = mode;
    }
    
    /**
     * Returns if this is in waiting state.
     * @return boolean
     */
    public boolean isWaiting(){
        return waiting;
    }
    
    /**
     * Sets this waiting state
     * @param w 
     */
    public void setWaiting(boolean w){
        waiting = w;
    }
    
    /*
     * Sends this players move to client outgoing socket channel. 
     * @throws IOException 
     */
    private void sendMove() throws IOException{
        networkChannel.outgoingNetworkMessage(Integer.toString(GUI.getSelectedRow()));
        networkChannel.outgoingNetworkMessage(Integer.toString(GUI.getSelectedColumn()));
    }
    
    /*
     * waits for server message via clients incoming socket channel 
     * @throws NumberFormatException
     * @throws IOException 
     */
    private void waitForInstruction() throws NumberFormatException, IOException{
        
        String message = networkChannel.incomingNetworkMessage();
        
        switch (message){
            
            case PLAYER1_WON:
                
                if(playerID == 2){                    
                    setMessage("Player 1 Won"+ "\nWAR IS OVER!" + "\nYou Lost");                    
                }else{                    
                    setMessage("YOU WON THE WAR!");                    
                }//end of if else block
                
                endGame();
                
                break;
            case PLAYER2_WON:
                
                if(playerID == 1){                    
                    setMessage("Player 2 Won"+ "\nWAR IS OVER!" + "\nYou Lost");
                }else{
                    setMessage("YOU WON THE WAR!");
                }//end of if else block
                
                endGame();
                
                break;
                
            case WAIT:
                recieveMove();
                setMessage("You have been hit!...");
                waitForInstruction();
                break;
                
            case CONTINUE:
                recieveMove();
                setTurn(true);
                setMessage("Your Move....");
                break;
                
            case START:
                setGameInPlay();
                setMessage("WAR HAS BEEN DECLARED!");
                if(getTurn())setMessage("Your Move....");
                else setMessage("Enemies Move Please Wait...");
                
                break;
                
            case HIT:
                GUI.setOpponentsCell(GUI.getSelectedRow(), 
                        GUI.getSelectedColumn(), GUI.getImage(message));
                setTurn(true);
                setMessage("Hit!");
                String hasWon = networkChannel.incomingNetworkMessage();
                if(hasWon.equalsIgnoreCase("END"))waitForInstruction();
                break;
                
            case MISS:
                GUI.setOpponentsCell(GUI.getSelectedRow(), 
                        GUI.getSelectedColumn(), GUI.getImage(message));
                setTurn(false);
                setMessage("Miss");
                setMessage("Enemies Move Please Wait...");
                waitForInstruction();
                break;
                
            case ERROR:
                if (playerID == 1)setMessage("Player 2 has quit\n waiting for player to join");
                else setMessage("Player 1 has quit\n waiting for player to join");
                endGame();
                break;
            default:
                throw new IOException();
        }//end of switch
        
    }//END OF waitForInstruction METHOD
    
    /**
     * Sets this turn state
     * @param turn 
     */
    public void setTurn(Boolean turn){
        this.turn = turn;
    }
    
    /**
     * Returns this turn state
     * @return boolean
     */
    public boolean getTurn(){
        return turn;
    }
    
    /**
     * Returns this game in play state.
     * @return boolean
     */
    public boolean isGameInPlay(){
        return gameInPlay;
    }
    
    /*
     * Sets this game in play state 
     */
    private void setGameInPlay(){
        gameInPlay = true;
        GUI.setGridEnabled(false, true);
        GUI.controlPanel.enableFleetButton(false);
    }
    
    /*
     * Ends this game. sets gameplay state and selection mode state to false 
     */
    private void endGame(){
        gameInPlay = false;
        setSelectionMode(false);
        GUI.controlPanel.enableStartButton(true);
    }
    
    /*
     * waits for other players move from server via clients incoming socket 
     * channel 
     * @throws NumberFormatException
     * @throws IOException 
     */
    private void recieveMove() throws NumberFormatException, IOException{
        int row = Integer.parseInt(networkChannel.incomingNetworkMessage());
        int column = Integer.parseInt(networkChannel.incomingNetworkMessage());
        String status = networkChannel.incomingNetworkMessage();
        GUI.setPlayersCell(row,column,GUI.getImage(status));
    }    

}//END OF CLASS
