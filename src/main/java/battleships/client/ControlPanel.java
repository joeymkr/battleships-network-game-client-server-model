package battleships.client;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * ControlPanel is a type of JPanel. To be added to JFrame. 
 * @author Joe Roberts
 */
public class ControlPanel extends JPanel {
    private ButtonGroup group = new ButtonGroup();
    private JRadioButton cSelector = new JRadioButton("Carrier");
    private JRadioButton sSelector = new JRadioButton("Submarine");
    private JRadioButton dSelector = new JRadioButton("Destroyer");
    private final GameBoard PARENT;
    private JButton positionFleet = new JButton("Battle Stations");
    private JButton connect = new JButton("Connect");
    private JButton startGame = new JButton("Start");
    private JTextArea message;
    private final JLabel TITLE = new JLabel("BATTLESHIPS");
    
    /**
     * Constructs and initialises this JPanels Components.
     * @param PARENT 
     * @see JPanel
     */
    public ControlPanel(GameBoard PARENT){
        
        //GameBoard callback function
        this.PARENT = PARENT;
        this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
        
        //fleetpanel to include three radiobuttons
        JPanel fleetPanel = new JPanel();
        fleetPanel.setLayout(new GridLayout(1,3));
        fleetPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));       
        fleetPanel.setMaximumSize(new Dimension(350, 200));
        
        //infoPanel to display textarea
        JPanel infoPanel = new JPanel();
        infoPanel.setLayout(new BoxLayout(infoPanel,BoxLayout.PAGE_AXIS));
        infoPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        
        //titlepanel to hold jlabel
        JPanel titlePanel = new JPanel();
        titlePanel.setMaximumSize(new Dimension(200, 50));
        
        //buttonpanel includes button group
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(1,1));
        buttonPanel.setMaximumSize(new Dimension(350, 50));
        
        //sets fleet panel radiobuttons
        dSelector.setActionCommand("Destroyer");
        dSelector.setSelected(true);
        sSelector.setActionCommand("Submarine");
        cSelector.setActionCommand("Carrier");
        
        //set infopanel textarea
        message = new JTextArea("Please connect to server");
        message.setFont(new Font("Serif", Font.PLAIN,16));
        message.setForeground(Color.GREEN);
        message.setBackground(Color.BLACK);
        message.setEditable(false);
        
        //adds textarea to scrollpane
        JScrollPane pane = new JScrollPane(message);
        
        //sets jlabel font
        TITLE.setFont(new Font("Serif", Font.BOLD,21));
        
        //sets battlestation button
        positionFleet.setFont(new Font("Ariel", Font.BOLD,10));
        positionFleet.setMaximumSize(new Dimension(175, 50));
        positionFleet.setEnabled(false);
        positionFleet.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                PARENT.CONTROLLER.setSelectionMode(false);
            }
        });
        
        //sets connect button
        connect.setFont(new Font("Ariel", Font.BOLD,11));
        connect.setMaximumSize(new Dimension(175, 50));
        connect.setEnabled(true);
        connect.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                PARENT.CONTROLLER.establishNetworkConnection();
            }
        });
        
        //sets start button
        startGame.setFont(new Font("Ariel", Font.BOLD,11));
        startGame.setMaximumSize(new Dimension(175, 50));
        startGame.setEnabled(false);
        startGame.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                PARENT.CONTROLLER.startGame();
            }
        });
        
        //groups radiobuttons
        group.add(dSelector);
        group.add(sSelector);
        group.add(cSelector);
        
        //adds components to specified JPanel
        titlePanel.add(TITLE);
        infoPanel.add(pane);
        fleetPanel.add(dSelector);
        fleetPanel.add(sSelector);
        fleetPanel.add(cSelector);
        buttonPanel.add(positionFleet);
        buttonPanel.add(connect);
        buttonPanel.add(startGame);
        
        //adds jpanels to this JPanel
        this.setSize(250, 250);
        this.add(titlePanel);        
        this.add(fleetPanel);
        this.add(buttonPanel);
        this.add(infoPanel);
        
    }//END OF CONSTRUCTOR
    
    /**
     * Sets submissionButtons enable state
     * @param s 
     */
    public void setSubmissionButton(boolean s){
        positionFleet.setEnabled(s);
    }
    
    /**
     * Sets start buttons enabled state
     * @param enabled 
     */
    public void enableStartButton(boolean enabled){
        startGame.setEnabled(enabled);
    }
    
    /**
     * Sets battle stations button enabled state
     * @param enabled 
     */
    public void enableFleetButton(boolean enabled){
        positionFleet.setEnabled(enabled);
    }
    
    /**
     * Appends JTextarea. Sets Caret to latest Caret to update JTextarea
     * @param message 
     */
    public void displayMessage(String message){
        this.message.append("\n" + message);
        this.message.setCaretPosition(this.message.getDocument().getLength());
    }
    
    /**
     * Returns selected radio button
     * @return int
     */
    public int getCurrentSelectedButton(){
        
        boolean selected =false;
        
        if ((selected = dSelector.isSelected())){
            
            return 0;
            
        } else if ((selected = sSelector.isSelected())){
            
            return 1;
            
        }else if ((selected = cSelector.isSelected())){
            
            return 2;
            
        } else {
            
            return -1;
            
        }//end of if else block
        
    }//END OF getCurrentSelectedButton METHOD
    
}//END OF CLASS
