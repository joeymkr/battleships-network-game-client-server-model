package battleships.client;

import java.awt.Point;

/**
 * A battleship representing a cell location and orientation/direction in a grid
 * @author Joe Roberts
 */
public class Battleship {
    private final String NAME;
    private int length;
    private int row;
    private int previousRow;
    private int column;
    private int previousColumn;
    private String orientation;
    private String previousOrientation;
    private boolean positioned = false;
    private boolean basePositionSet = false;
    
    /**
     * Constructs and initialises a battleship with name: UNKNOWN
     */
    public Battleship(){
        this("UNKNOWN");
    }
    
    /**
     * Constructs and initialises a battleship with the same value of the 
     * specified String Object and sets default length value
     * @param name a String
     */
    public Battleship(String name){
        this(name, 2);
    }//END OF CONSTRUCTOR
    
    /**
     * Constructs and initialises a battleship with the same value of
     * specified String Object and length value in a grid system.
     * @param name a String
     * @param length grid length
     */
    public Battleship(String name, int length){
        this.NAME = name;
        this.length = length;
    }//END OF CONSTRUCTOR
    
    /**
     * Returns the name value of this battleship Object
     * @return a copy of this name
     */
    public String getName(){
        return NAME;
    }
    
    /**
     * Sets the grid location of this battleship to the specified location
     * @param row an int representing a grid row
     * @param column an int representing a grid column
     */
    public void setBasePosition(int row, int column){
        this.row = row;
        this.column = column;
    }
    
    /**
     * Sets battleship to fully positioned in grid or not
     * @param isSet new value for this positioned value
     */
    public void setFullyPositioned(boolean isSet){
        positioned = isSet;
    }
    
    /**
     * Returns whether or not battleship is fully positioned in grid. Specific 
     * to both base position and orientation being set.
     * @return true if fully positioned. false if not.
     */
    public boolean isPositioned(){
        return positioned;
    }
    
    /**
     * Sets battleships base position set to true (base position set) or
     * false (base position not set)
     * @param isSet new value for base position boolean
     */
    public void setIsBasePositionSet(boolean isSet){
        basePositionSet = isSet;
    }
    
    /**
     * Returns whether or not base position within grid is set for this 
     * battleship. Does not represent battleships fully positioned state.
     * @return value of this battleships base position set state
     */
    public boolean isBasePositionSet(){
        return basePositionSet;
    }
    
    /**
     * Sets battleships orientation or direction to specified value
     * @param direction new value for orientation string variable
     */
    public void setOrientation(String direction){
        orientation = direction;
    }
    
    /**
     * Returns battleships orientation in grid system
     * @return copy of string orientation value
     */
    public String getOrientation(){
        return orientation;
    }
    
    /**
     * Returns point of this battleships grid base position.
     * @return new point at same location as battleships grid row and column
     */
    public Point getBasePosition(){
        return new Point(row, column);
    }
    
    /**
     * Returns grid length of this battleship
     * @return copy of this battleships grid length
     */
    public int getLength(){
        return length;
    }
    
    /**
     * Sets new grid position and orientation to this battleships specified
     * grid position
     * @param r new value to previous grid row
     * @param c new value to previous grid column
     * @param o new value to previous orientation
     */
    public void setPreviousPosition(int r, int c, String o){
        this.previousRow = r;
        this.previousColumn = c;
        this.previousOrientation = o;
    }
    
    /**
     * Sets new grid position and orientation to this battleships specified
     * grid position
     * @param pos new value to previous grid row & column
     * @param o new value to previous orientation
     */
    public void setPreviousPosition(Point pos, String o){
        setPreviousPosition((int)pos.getX(),(int)pos.getY(),o);
    }
    
    /**
     * Returns point of battleships previous position in grid system
     * @return copy of battleships previous grid row and column
     */
    public Point getPreviousPosition(){
        return new Point(previousRow,previousColumn);
    }
    
    /**
     * Returns battleships previous grid orientation
     * @return copy of battleships previous orientation string
     */
    public String getPreviousOrientation(){
        return previousOrientation;
    }

}//END OF CLASS
