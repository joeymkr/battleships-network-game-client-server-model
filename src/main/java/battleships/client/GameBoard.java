package battleships.client;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * Type of JFrame create Graphical User Interface for battleship Controller
 *
 * @author Joe Roberts
 * @see Controller
 */
public class GameBoard extends JFrame {

    private final int gridSize = 6;
    private Cell[][] playersGrid = new Cell[gridSize][gridSize];
    private Cell[][] opponentsGrid = new Cell[gridSize][gridSize];
    private int selectedRow;
    private int selectedColumn;
    private int mousePosX = 0;
    private int mousePosY = 0;
    private String currentDirection = "";
    private JPanel playersBoard = new JPanel();
    private JPanel opponentsBoard = new JPanel();    
    private BufferedImage endDown;
    private BufferedImage endUp;
    private BufferedImage endLeft;
    private BufferedImage endRight;
    private BufferedImage middle;
    private BufferedImage middleHorizontal;
    private BufferedImage empty;
    private BufferedImage miss;
    private BufferedImage hit;

    /**
     * Call back instance
     */
    public final Controller CONTROLLER;

    /**
     * Controller access
     */
    public ControlPanel controlPanel = new ControlPanel(this);

    /**
     * Constructs and initialises JFrame Components. Initiates call back
     * controller reference.
     *
     * @param parent
     */
    public GameBoard(Controller parent) {
        CONTROLLER = parent;
        initBoard();
        this.setSize(1000, 400);
        this.setResizable(false);
        this.setLayout(new GridLayout(0, 3, 0, 0));
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }//END OF CONSTRUCTOR

    /**
     * Initiates components and adds to this JFrame
     */
    public void initBoard() {
        try{
            endDown = ImageIO.read(new File("resources/end-down.jpg"));
            endUp = ImageIO.read(new File("resources/end-up.jpg"));
            endLeft = ImageIO.read(new File("resources/end-left.jpg"));
            endRight = ImageIO.read(new File("resources/end-right.jpg"));
            middle = ImageIO.read(new File("resources/middle.jpg"));
            middleHorizontal = ImageIO.read(new File("resources/middle-horizontal.jpg"));
            empty = ImageIO.read(new File("resources/empty.jpg"));
            miss = ImageIO.read(new File("resources/miss.jpg"));
            hit = ImageIO.read(new File("resources/hit.jpg"));
        }catch(IOException ioe){
            System.out.println("Cannot open image file");
        }

        playersBoard.setLayout(new GridLayout(gridSize, gridSize, 0, 0));
        playersBoard.setSize(500, 500);
        opponentsBoard.setLayout(new GridLayout(gridSize, gridSize, 0, 0));
        opponentsBoard.setSize(500, 500);

        for (int i = 0; i < gridSize; i++) {

            for (int n = 0; n < gridSize; n++) {
                playersBoard.add(playersGrid[i][n] = new Cell(empty,i, n, this, false));
            }//end of inner for loop

            for (int p = 0; p < gridSize; p++) {
                opponentsBoard.add(opponentsGrid[i][p] = new Cell(empty,i, p, this, false));
            }//end of inner for loop

        }//end of outer for loop

        this.add(playersBoard);
        this.add(controlPanel);
        this.add(opponentsBoard);

    }//END OF initBoard METHOD

    /**
     * Returns current mouse Direction. Used in conjunction with battleships
     * orientation in grid system
     *
     * @return String
     */
    public String getCurrentMouseDirection() {
        return currentDirection;
    }

    /**
     * Selects grid element with row, column representations
     *
     * @param row selects grid row
     * @param column selects grid column
     */
    public void selectCell(int row, int column) {
        selectedRow = row;
        selectedColumn = column;
    }

    /**
     * Sets specified cell to specified status value
     *
     * @param row specifies grid row
     * @param column specifies grid column
     * @param image represents cell status
     */    
    public void setPlayersCell(int row, int column, BufferedImage image) {
        try {
            playersGrid[row][column].setCellStatus(image);
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    /**
     * Sets specified cell to specified status value
     *
     * @param row specifies grid row
     * @param column specifies grid column
     * @param image represents cell status
     */    
    public void setOpponentsCell(int row, int column, BufferedImage image) {
        opponentsGrid[row][column].setCellStatus(image);
    }

    /**
     * Returns current selected row
     *
     * @return int
     */
    public int getSelectedRow() {
        return selectedRow;
    }

    /**
     * Returns current selected column
     *
     * @return int
     */
    public int getSelectedColumn() {
        return selectedColumn;
    }

    /**
     * returns current selected radio button
     *
     * @return int
     */
    public int getCurrentSelection() {
        return controlPanel.getCurrentSelectedButton();
    }

    /**
     * runs new thread
     */
    public void animate() {
        Animation animate = new Animation();
        Thread animation = new Thread(animate);
        animation.start();
    }

    /**
     * Returns this JFrame
     *
     * @return JFrame
     */
    public JFrame getComponent() {
        return this;
    }

    /**
     * Sets mouse X position relative to specified JFrame JPanel
     *
     * @param x
     */
    public void setMousePosX(int x) {
        mousePosX = x;
    }

    /**
     * Sets mouse Y position relative to specified JFrame JPanel
     *
     * @param y
     */
    public void setMousePosY(int y) {
        mousePosY = y;
    }

    /**
     * Returns mouse X position relative to specified JFrame JPanel
     *
     * @return int
     */
    public int getMousePosX() {
        return mousePosX;
    }

    /**
     * Returns mouse Y position relative to specified JFrame JPanel
     *
     * @return int
     */
    public int getMousePosY() {
        return mousePosY;
    }
    
    /**
     * Returns type of image based on cell status
     * @param image bufferedImage
     * @return 
     */
    public BufferedImage getImage(String image){
        BufferedImage temp = empty;
        switch (image){
            case "EMPTY":
                temp = empty;
                break;
            case "MISS":
                temp = miss;
                break;
            case "HIT":
                temp = hit;
                break;
            default:
                break;
        }
        return temp;
    }
    
    /**
     * unlocks cell
     * @param r
     * @param c 
     */
    public void unlockCell(int r, int c){
        playersGrid[r][c].setLock(false);
    }
    
    /**
     * locks cell
     * @param r
     * @param c 
     */
    public void lockCell(int r, int c){
        playersGrid[r][c].setLock(true);
    }

    /**
     * Sets both grids editable state to specified values.
     *
     * @param grid1 player 1 editable value
     * @param grid2 player 2 editable value
     */
    public void setGridEnabled(boolean grid1, boolean grid2) {

        for (int i = 0; i < gridSize; i++) {

            for (int n = 0; n < gridSize; n++) {
                playersGrid[i][n].setClickable(grid1);
                playersGrid[i][n].setLock(false);
                //setPlayersCell(i, n, "UNLOCK");
            }//end of inner for loop

            for (int p = 0; p < gridSize; p++) {
                opponentsGrid[i][p].setClickable(grid2);
                opponentsGrid[i][p].setLock(false);
                //setOpponentsCell(i, p, "UNLOCK");
            }//end of inner for loop

        }//end of outer for loop

    }//END OF setGridEnabled METHOD

    /**
     * Resets and clears both grids. sets enabled state to false
     */
    public void resetGrids() {

        for (int i = 0; i < gridSize; i++) {

            for (int n = 0; n < gridSize; n++) {
                setPlayersCell(i, n, empty);
            }//end of inner for loop

            for (int p = 0; p < gridSize; p++) {
                setOpponentsCell(i, p, empty);
            }//end of inner for loop

        }//end of outer for loop

        setGridEnabled(false, false);

    }//END OF resetGrids METHOD

    /**
     * Paints all Cells unoccupied relative to the current selected Cell and
     * mouse orientation in JPanel
     *
     * @param row current grid row
     * @param column current grid column
     * @param direction mouse orientation
     */
    public void paintUnoccupiedCells(int row, int column, String direction) {

        if (direction.equalsIgnoreCase("NORTH")) {

            for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {

                        //paints east
                setPlayersCell(row, column + i, empty);
                        //paints west
                setPlayersCell(row, column - i, empty);
                        //paints south
                setPlayersCell(row + i, column, empty);
                
            }//end of for loop

        } else if (direction.equalsIgnoreCase("EAST")) {

            for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {

                        //paints north
                setPlayersCell(row - i, column, empty);
                        //paints west
                setPlayersCell(row, column - i, empty);
                        //paints south
                setPlayersCell(row + i, column, empty);

            }//end of for loop

        } else if (direction.equalsIgnoreCase("SOUTH")) {

            for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {

                        //paints north
                setPlayersCell(row - i, column, empty);
                        //paints west
                setPlayersCell(row, column - i, empty);
                        //paints east
                setPlayersCell(row, column + i, empty);

            }//end of for loop

        } else {

            for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {

                //paints north
                setPlayersCell(row - i, column, empty);
                //paints south
                setPlayersCell(row + i, column, empty);
                //paints east
                setPlayersCell(row, column + i, empty);
                
            }//end of for loop
            
        }//end of if else block
        
        currentDirection = direction;
        
    }//END OF paintUnoccupiedCells METHOD

    /*
     * Inner class. Used to animate cells in conjunction with mouse position,
     * orientation and selected cells during selection mode true
     */
    private class Animation implements Runnable {

        private Point shipBasePos = CONTROLLER.fleet[getCurrentSelection()].getBasePosition();
        private int basePosX = (int) shipBasePos.getX();
        private int basePosY = (int) shipBasePos.getY();
        private int indexCounter = 0;

        @Override
        public void run() {
            try {
                while (!CONTROLLER.fleet[getCurrentSelection()].isPositioned()) {

                    int x = mousePosX;
                    int y = mousePosY;
                    
                    Point gridPos = playersGrid[basePosX][basePosY].getLocation();
                    int cellXcentre = (int)(gridPos.getX()+(playersGrid[0][0].getCellDimension().width / 2));
                    int cellYcentre = (int)(gridPos.getY()+(playersGrid[0][0].getCellDimension().width / 2));
                    
                    Point basePoint = new Point(cellXcentre, cellYcentre);
                    ArrayList<Point> shipCells = new ArrayList<Point>();
                    shipCells.add(shipBasePos);
                    int difX = x - (int) basePoint.getX();
                    int difY = y - (int) basePoint.getY();

                    if (y < basePoint.getY()) {
                        //if in northern hemisphere

                        if (x <= basePoint.getX()) {
                            //if in northwestern hemisphere

                            if (difX < difY) {
                                //swing west
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX(), (int) shipBasePos.getY() - i));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goWest(0);
                                }
                            } else {
                                //swing north
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX() - i, (int) shipBasePos.getY()));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goNorth(0);
                                }
                            }

                        } else {
                            //in northeastern hemisphere
                            if (difX > difY) {
                                //swing east
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX(), (int) shipBasePos.getY() + i));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goEast(0);
                                }
                            } else {
                                //swing north
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX() - i, (int) shipBasePos.getY()));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goNorth(0);
                                }
                            }
                        }

                    } else {
                        //in southern hemisphere

                        if (x <= basePoint.getX()) {
                            //in southwestern hemisphere

                            if (difX > difY) {
                                //swing west
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX(), (int) shipBasePos.getY() - i));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goWest(0);
                                }
                            } else {
                                //swing south
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX() + i, (int) shipBasePos.getY()));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goSouth(0);
                                }
                            }

                        } else {
                            //in southeastern hemisphere
                            if (difX > difY) {
                                //swing east
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX(), (int) shipBasePos.getY() + i));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goEast(0);
                                }
                            } else {
                                //swing south
                                for (int i = 1; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                                    shipCells.add(new Point((int) shipBasePos.getX() + i, (int) shipBasePos.getY()));
                                }
                                if (checkCellAvailability(shipCells)) {
                                    goSouth(0);
                                }
                            }
                        }
                    }
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                controlPanel.displayMessage("Can not place ship here!");
            }
        }

        private void goWest(int loopCounter) {
            BufferedImage temp = middle;
            if (loopCounter >= 4) {
                controlPanel.displayMessage("can not place ship here");
                CONTROLLER.resetShip();
            }
            int posCounter = 0;
            try {
                for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                    
                    if(i == 0 || i == (CONTROLLER.fleet[getCurrentSelection()].getLength()-1)){
                        
                        if(i == 0)temp = endRight;
                        else temp = endLeft;
                        
                    }else{
                        temp = middleHorizontal;
                    }
                    setPlayersCell(basePosX, basePosY - posCounter, temp);
                    posCounter++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                goNorth(loopCounter + 1);
            }
            if (!currentDirection.equalsIgnoreCase("WEST")) {
                paintUnoccupiedCells(basePosX, basePosY, "WEST");
            }

        }

        private void goNorth(int loopCounter) {
            BufferedImage temp = middle;
            if (loopCounter >= 4) {
                controlPanel.displayMessage("can not place ship here");
                CONTROLLER.resetShip();
            }
            int posCounter = 0;
            try {
                for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                    
                    if(i == 0 || i == (CONTROLLER.fleet[getCurrentSelection()].getLength()-1)){
                        
                        if(i==0)temp = endDown;
                        else temp = endUp;
                        
                    }else{
                        temp = middle;
                    }
                    
                    setPlayersCell(basePosX - posCounter, basePosY, temp);
                    posCounter++;
                }
                if (!currentDirection.equalsIgnoreCase("NORTH")) {
                    paintUnoccupiedCells(basePosX, basePosY, "NORTH");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                goSouth(loopCounter + 1);
            }
        }

        private void goSouth(int loopCounter) {
            BufferedImage temp = middle;
            if (loopCounter >= 4) {
                controlPanel.displayMessage("can not place ship here");
                CONTROLLER.resetShip();
            }
            int posCounter = 0;
            try {
                for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                    
                    if(i == 0 || i == (CONTROLLER.fleet[getCurrentSelection()].getLength()-1)){
                        
                        if(i==0)temp = endUp;
                        else temp = endDown;
                        
                    }else{
                        temp = middle;
                    }
                    
                    setPlayersCell(basePosX + posCounter, basePosY, temp);
                    posCounter++;
                }
                if (!currentDirection.equalsIgnoreCase("SOUTH")) {
                    paintUnoccupiedCells(basePosX, basePosY, "SOUTH");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                goEast(loopCounter + 1);
            }
        }

        private void goEast(int loopCounter) {
            BufferedImage temp = middle;
            if (loopCounter >= 4) {
                controlPanel.displayMessage("can not place ship here");
                CONTROLLER.resetShip();
            }
            int posCounter = 0;
            try {
                for (int i = 0; i < CONTROLLER.fleet[getCurrentSelection()].getLength(); i++) {
                    
                    if(i == 0 || i == (CONTROLLER.fleet[getCurrentSelection()].getLength()-1)){
                        
                        if(i==0)temp = endLeft;
                        else temp = endRight;
                        
                    }else{
                        temp = middleHorizontal;
                    }
                    
                    setPlayersCell(basePosX, basePosY + posCounter, temp);
                    posCounter++;
                }
                if (!currentDirection.equalsIgnoreCase("EAST")) {
                    paintUnoccupiedCells(basePosX, basePosY, "EAST");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                goWest(loopCounter + 1);
            }
        }

        private boolean checkCellAvailability(ArrayList<Point> cells) {
            for (Point p : cells) {
                try {
                    if (playersGrid[(int) p.getX()][(int) p.getY()].isLocked()) {

                        indexCounter++;
                        return false;
                    }
                } catch (ArrayIndexOutOfBoundsException iob) {
                    indexCounter++;
                    return false;
                }
            }
            indexCounter = 0;
            return true;
        }
    }//END OF INNER CLASS
}//END OF CLASS
