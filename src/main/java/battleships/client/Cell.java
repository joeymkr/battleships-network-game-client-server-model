package battleships.client;

import battleships.common.NetworkMessages;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

/**
 * Cell is a type of JPanel and MouseMotionListener. To be added to JFrame in 
 * grid type system.
 * @author Joe Roberts
 * @see JPanel
 * @see MouseMotionListener
 */
public class Cell extends JPanel implements NetworkMessages, MouseMotionListener{
    private final int ROW;
    private final int COLUMN;
    private final GameBoard PARENT;
    private Color cellGraphic;
    private String status = "EMPTY";
    private boolean clickable;
    private boolean locked;
    private BufferedImage currentStatus;
        
    /**
     * Constructs and initialises new cell with the specified grid row, column,
     * callback GameBoard and editable value.
     * @param cstatus
     * @param row the row to place this cell in grid
     * @param column the column to place this cell in grid
     * @param board the calling GameBoard for callback
     * @param clickable sets this panels editable state
     */
    public Cell (BufferedImage cstatus, int row, int column, GameBoard board, boolean clickable){
        this.ROW = row;
        this.COLUMN = column;
        this.PARENT = board;
        this.cellGraphic = new Color(0,100,175);
        this.clickable = clickable;
        locked = false;
        setBorder(new LineBorder(Color.black, 1));
        this.setBackground(cellGraphic);
        this.addMouseListener(new ClickListener());
        addMouseMotionListener(this);
        
        currentStatus = cstatus;
        
    }//END OF CONSTRUCTOR
    
    /**
     * Overrides MouseMotionListener method. NOT USED IN THIS CASE.
     * @param e mouse event
     */
    @Override
    public void mouseDragged(MouseEvent e) {
    
    }
    
    /**
     * Overrides MouseMotionListener method. Records current mouseLocation (x,y)
     * point in relation to parent JFrame JPanel
     * @param e mouseEvent
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        if (PARENT.CONTROLLER.inSelectionMode()){
            Container container = PARENT.getContentPane();
            Point mouseLocation = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), container);
            PARENT.setMousePosX((int)mouseLocation.getX());
            PARENT.setMousePosY((int)mouseLocation.getY());
        }
    }
    
    /**
     * InnerClass added to outer classes JPanel. controls JPanels mouseClick
     * events
     */
    private class ClickListener extends MouseAdapter{
        
        /**
         * Overrides MouseAdapter method
         * @param e MouseEvent
         * @see MouseAdapter
         */
        @Override
        public void mouseClicked(MouseEvent e){
            
            //is this JPanel editable. prevents unwanted clicks
            if (isClickable()){
                
                //is this game in gameplay mode && is this JPanel selectable
                if(PARENT.CONTROLLER.inSelectionMode() && !isLocked()){
                    
                    //selects this cell
                    PARENT.selectCell(ROW,COLUMN);
                    
                    //is currently selected battleship base positioned in grid
                    //and not fully position in said grid
                    if (PARENT.CONTROLLER.fleet
                            [PARENT.getCurrentSelection()].isBasePositionSet() && 
                            !PARENT.CONTROLLER.fleet
                            [PARENT.getCurrentSelection()].isPositioned()){
                        
                        //fully positions ship in grid
                        PARENT.CONTROLLER.placeShip();
                    
                    //is currently selected battleship fully positioned in grid
                    }else if(PARENT.CONTROLLER.fleet
                            [PARENT.getCurrentSelection()].isPositioned()){
                        
                        //repositions ship to this cell
                        PARENT.CONTROLLER.resetShip();
                        mouseClicked(e);
                    
                    }else{
                        
                        //sets currently selected ships position to this cell
                        PARENT.CONTROLLER.setCurrentShipBasePosition();
                        PARENT.animate();
                    }//end of inner if else block
                    
                //if this cells status is empty and this parent controller has
                //has turn and parent controller game is in play
                }else if (status.equalsIgnoreCase("EMPTY") 
                        && PARENT.CONTROLLER.getTurn() && PARENT.CONTROLLER.isGameInPlay()){
                    
                    //selects this cell
                    PARENT.selectCell(ROW, COLUMN);
                    PARENT.CONTROLLER.setTurn(false);
                    PARENT.CONTROLLER.setWaiting(false);
                    
                }
            }
        }//END OF mouseClicked METHOD
        
    }//END OF INNER CLASS
    
    /**
     * Returns this JPanels editable state
     * @return true if editable, false if not
     */
    public boolean isClickable(){
        return clickable;
    }
    
    /**
     * Sets this JPanels editable state
     * @param clickable new value for this editable state value
     */
    public void setClickable(boolean clickable){
        this.clickable = clickable;
    }
    
    /**
     * Sets this Cells lock state
     * @param lock new value for this Cells lock state value
     */
    public void setLock(boolean lock){
        locked = lock;
    }
    
    /**
     * Returns this Cells lock state
     * @return true if locked, false if not
     */
    public boolean isLocked(){
        return locked;
    }
    
    /**
     * Sets this Cells status and graphic
     * @param cstatus
     */
    public void setCellStatus(BufferedImage cstatus){
        
        Color color = new Color(0,100,175);
        cellGraphic = color;
        this.setBackground(color);
        
        if(!locked){
            currentStatus = cstatus;
            repaint();
        }
        
    }//END OF setCellStatus METHOD
            
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.drawImage(currentStatus, 0, 0,60,60, this);
    }
    
    /**
     * Returns this Cells current graphic. Does not represent Cells actual 
     * status
     * @return Color
     */
    public Color getCellGraphic(){
        return cellGraphic;
    }
    
    /**
     * Returns Cells grid row
     * @return int
     */
    public int getRow(){
        return ROW;
    }
    
    /**
     * Returns Cells grid column
     * @return int
     */
    public int getColumn(){
        return COLUMN;
    }
    
    /**
     * Returns Cells occupied state
     * @return boolean
     */
    public boolean isOccupied(){
        return status.equalsIgnoreCase(OCCUPIED);
    }
    
    public Dimension getCellDimension(){
        return new Dimension(this.getWidth(),this.getHeight());
    }
}
