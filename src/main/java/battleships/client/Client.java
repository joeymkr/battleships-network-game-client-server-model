package battleships.client;

import battleships.common.NetworkMessages;
import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;

/**
 * Client establishes network socket connection. Allows client/server type
 * communications via PrintWriter, BufferedReader(InputStreamReader).
 * @author Joe Roberts
 * @see PrintWriter
 * @see BufferedReader
 * @see InputStreamReader
 * @see Socket
 */
public class Client implements NetworkMessages {

    private BufferedReader incoming;
    private PrintWriter outgoing;
    private Socket socket;
    
    /**
     * Empty constructor. Call .connect to attempt connection
     */
    public Client() {
        //null constructor 
    }

    /**
     * Returns result of socket connection attempt. If Socket is successful
     * input and output communication streams are created.
     * @return true if successful, otherwise false
     */
    public boolean connect() {
        try {
            
            socket = new Socket(InetAddress.getLocalHost(), 8000);

            incoming = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));

            outgoing = new PrintWriter(socket.getOutputStream());
                        
            return true;

        } catch (SocketException ce) {            
            JOptionPane.showMessageDialog(null,
                    "Unable to connect to server");            
            return false;
            
        } catch (IOException ioe) {            
            JOptionPane.showMessageDialog(null,
                    "Error with incoming or outgoing channels");            
            return false;
        }//end of try catch block
        
    }//END OF connect METHOD
    
    /**
     * Sets socket to null
     */
    public void resetConnection() {
        socket = null;
    }
    
    /**
     * Checks whether input communication channel is ready.
     * @return true if channel is ready, otherwise false
     * @throws SocketException
     * @throws IOException 
     */
    public boolean checkReaderStatus() throws SocketException, IOException {
        return incoming.ready();
    }

    /**
     * Returns incoming String communication
     * @return String network message
     * @throws IOException 
     */
    public String incomingNetworkMessage() throws IOException {
        return incoming.readLine();
    }

    /**
     * Checks socket connection status
     * @return true if socket is connected, otherwise returns false
     */
    public boolean isConnected() {
        boolean result;
        try {
            result = socket.isConnected();
        } catch (NullPointerException npe) {
            result = false;
        }
        return result;
    }

    /**
     * Sends String through socket output channel.
     * @param message String to send
     * @throws SocketException 
     */
    public void outgoingNetworkMessage(String message) throws SocketException {
        outgoing.println(message);
        outgoing.flush();

        if (outgoing.checkError()) {
            throw new SocketException();
        }
    }
    
}//END OF CLASS
