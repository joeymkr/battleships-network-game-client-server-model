package battleships.common;

/**
 * Network Protocol for Battleships. Defines a set of Strings to be used in 
 * network message system.
 * @author Joe Roberts
 */
public interface NetworkMessages {
    public static String PLAYER1ID = "PLAYER1";
    public static String PLAYER2ID = "PLAYER2";
    public static String PLAYER1_WON = "PLAYER1_WON";
    public static String PLAYER2_WON = "PLAYER2_WON";
    public static String CONTINUE = "CONTINUE";
    public static String WAIT = "WAIT";
    public static String START = "START";
    public static String END = "END";
    public static String ERROR = "ERROR";
    
    public static String EMPTY = "EMPTY";
    public static String MISS = "MISS";
    public static String HIT = "HIT";
    public static String OCCUPIED = "OCCUPIED";
    
    public static String LOCK = "LOCK";
    public static String UNLOCK = "UNLOCK";
    
    public static String NORTH = "NORTH";
    public static String SOUTH = "SOUTH";
    public static String WEST = "WEST";
    public static String EAST = "EAST";
}
